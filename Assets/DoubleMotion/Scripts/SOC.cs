﻿using UnityEngine;

public static class SOC {
	private static GameObject[] objects;

	public static GameObject GetObject(int _id) {
		if (objects.Length < _id)
			return null;
		return objects[_id];
	}

	public static void SetObjects(GameObject[] _objects) {
		objects = _objects;
	}
}

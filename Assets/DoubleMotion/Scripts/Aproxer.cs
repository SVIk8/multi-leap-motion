﻿using System.Collections.Generic;
using UnityEngine;

public class Aproxer : MonoBehaviour {
	public Transform[] finalResault;
	private List<Transform[]> sources = new List<Transform[]>();

	public void AddSource(Transform[] _source) {
		sources.Add(_source);
	}

	private void Update() {
		if (sources.Count == 0)
			return;

		foreach (Transform t in finalResault) {
			t.position = Vector3.zero;
			t.rotation = Quaternion.identity;
		}

		for (int i = 0; i < sources.Count; ++i) {
			for (int j = 0; j < sources[i].Length; ++j) {
				finalResault[j].localPosition = Vector3.Lerp(sources[i][j].localPosition, finalResault[j].localPosition, i / (float)sources.Count);
				finalResault[j].localRotation = Quaternion.Lerp(sources[i][j].localRotation, finalResault[j].localRotation, i / (float)sources.Count);
			}
		}
	}
}
